#pragma once
#include <cstdint>
#include <initializer_list>
#include <vector>
#include <memory>

namespace Concepts
{
	// ������� �����/sbox/...
	using ElementType = uint8_t;

	// ��� ������ ��� �������� �������� ����-���� (��� ������� ��������)
	//  - �������������
	//  - �����������
	//  - ��������, ����������� ��� ��������� ���� ��������� ������
	//  - ������� �������� �����������
	using SizeType = size_t;


	// �������� � ������������ ��������
	//  - ��� �������������, ������� ����������� �������������� ������
	//  - �������� ��� �������� �� ���������� ������� ��������
	template<class T>
	using RAIterator = T * ;


	// ����������� �������� � ������������ ��������
	//  - ��� �������������, ����������� ����������� �������������� ������
	//  - �������� ��� �������� �� ���������� ������� ��������
	template<class T>
	using RAIteratorConst = const T *;


	// ��� ������ ��� �������� ����� �������������� �������
	//  - ����������� ����������� ������������� ��������� ������� �����. ������ ����� ��������������� � ������ �������� �������
	//  - ��������� ����������� �� ���������
	//  - ������ �������� ������������
	//  - ������� ������������ ������ �� ���� ��������� �����
	template<class T>
	class Block
	{
	public:
		// ��� �������� ���������
		using value_type = T;
		// ��� ��� �������� �������
		using size_type = size_t;
		// ��� ������ �� �������
		using reference = T & ;
		// ��� ����������� ������ �� �������
		using const_reference = const T &;

		// ����������� �� ���������
		Block();
		// ����������� �������� ������ �����
		explicit Block(size_type block_size);
		// ����������� � ��������������
		Block(size_type block_size, const T & value);
		// ����������� ����������� ���������������� ������
		//  - � ����������� �� ���������� - ������ �������� ����� �� ������� � ���������
		Block(std::initializer_list<T> init);

		// �������� ��������� �� ������
		T * data();
		const T * data() const;

		// ������������ ������ � ���������
		reference operator[](size_type index);
		const_reference operator[](size_type index) const;

		// �������� ������ �����
		size_type size() const;
	};


	// ��� ������ ��� �������� sbox'�
	//  - �������� ������ ���������������� �����������. ���������� ����� ������������ �������������� ������������
	//  - ������� ������������ ������ �� ���� ��������� sbox'�
	template<class T>
	class Sbox
	{
	public:
		// ��� �������� ���������
		using value_type = T;
		// ��� ��� �������� �������
		using size_type = size_t;
		// ��� ����������� ������ �� �������
		using const_reference = const T &;

		// ������������ ������ � ���������
		const_reference operator[](size_type index) const;

		// �������� ������ �����
		size_type size() const;
	};

	// ��� ������ ��� ��������� ���������� � ����� ��������� �� dX � dY
	//  - ���������� ������������ Sbox'��
	//  - ����� ������ ������������ ���������� ��������� �� dX � dY
	template<class SboxType>
	class Ddt
	{
	public:
		// ��� �������� ���������
		using delta_type = SboxType::value_type;
		// ��� ��� �������� �������
		using size_type = size_t;
		// ����� �������
		struct any_type {};
		any_type any;

		// �������� ������������ ���������� ��������� �� dX � dY
		size_type operator()(delta_type dX, delta_type dY) const;
		size_type operator()(any_type any, delta_type dY) const;
		size_type operator()(delta_type dX, any_type any) const;
		size_type operator()(any_type any1, any_type any2) const; // ����� ������� Sbox

		// ������ sbox (������������ ��������� ���-�� ���������)
		size_type total() const;
	};


	// �������� ��������������
	class LinearTransform
	{
	public:
		// ��������� �������� ��������������
		template<class InputIter, class OutputIter>
		void operator()(const InputIter & input, const OutputIter & output) const;
	};

	// ���������������� ����
	template<class BlockType = Block<ElementType>>
	struct Path
	{
		BlockType input_block;
		BlockType output_block;
	};

	// ������������
	template<class BlockType = Block<ElementType>>
	struct Path
	{
		BlockType delta_x;
		std::vector<BlockType> input_blocks;
		std::vector<BlockType> output_blocks;
		BlockType delta_y;
	};

	// ������������
	//  - ���������� �������� {0,1}
	class Configuration
	{
	public:
		// ��� �������� ���������
		using value_type = T;
		// ��� ��� �������� �������
		using size_type = size_t;
		// ��� ������ �� �������
		using reference = T & ;
		// ��� ����������� ������ �� �������
		using const_reference = const T &;

		// ������������ ������ � ���������
		reference operator[](size_type index);
		const_reference operator[](size_type index) const;

		// �������� ������ ����������
		size_type size() const;
	};

	// ������� ��� ������ � ��������������
	namespace configuration
	{
		// ��� ������������ (����� ��������� ���������)
		template<class Configuration>
		size_t weight(const Configuration & conf);

		// ������������� ������������ � �����
		template<class Number, class Configuration>
		void get_state(const Configuration & conf, Number & state);

		// ������������� ����� � ������������
		template<class Number, class Configuration>
		void set_state(Number state, Configuration & conf);

		// ������������� ������� � �������� ���� � ������������
		template<class Block, class Configuration>
		void set_state(const Block & input_block, const Block & output_block, Configuration & conf);

		// ������������ ����� �� ������� � ����������� � �������� �������������
		template<class Configuration, class Iterator, class Block>
		void restore_block(const Configuration & conf, const Iterator arr, Block & input_block, Block & output_block);

		// ������� �������� ������ �� �������� ������������
		template<class Configuration, class Block, class Iterator>
		void extract_elements(const Configuration & conf, const Block & input_block, const Block & output_block, Iterator arr);
	}

	// ����������� �������� �� dX � dY
	template<class ProbabilityType, class DdtType>
	class TransitionProbability
	{
	public:
		ProbabilityType dX_to_dY() const;
	};

	// ������� �������� ���������
	template<class ElementType>
	class LinearSystem
	{
	public:
		// ����������� ����������� ������� �������������� ������� �������� ���������
		// ������� elements ������� rows * cols
		// ������� elements ����� ��������������!
		LinearSystem(std::unique_ptr<ElementType[]> elements, size_t rows, size_t cols);

		// ���������� ����������
		size_t count_vars() const;
		// ���������� ��������� ����������
		size_t count_free_vars() const;
		// ���������� ��������� ���������� 
		size_t count_depend_vars() const;

		// �������� �������
		template<class InputIter, class OutputIter>
		void get_solution(InputIter free_vars, OutputIter result) const;
	};
}