#pragma once
#include <cstdint>

// Структура описывающая произвольный LSX шифр
struct ByteCrypter
{
	size_t block_size; 
	const uint8_t * sbox;
	const uint8_t * l_matrix;
	uint32_t gf2_module;
};
