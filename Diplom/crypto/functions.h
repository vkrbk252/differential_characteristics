#pragma once
#include <cassert>
#include "block.h"
#include "configuration/block_configuration.h"
#include "path/path.h"
#include "configuration/path_configuration.h"
#include "linear_transform.h"
#include "differential/differential.h"
#include "transition_probability.h"


//
//	Работа с числами заданного веса
//

// Количество единичных битов в числе
template<class Number>
inline size_t count_bits(Number n)
{
	size_t count = 0;
	while (n != 0) {
		count++;
		n = n & (n - 1);
	}
	return count;
}

// Создать стартовую конфигурацию пути заданного веса
template<class Number>
Number start_state(size_t weight)
{
	Number state = 0;
	while (count_bits(state) != weight)
	{
		state++;
	}
	return state;
}

// Последнее существующее состояние
template<class Number>
Number end_state(size_t weight, Number max)
{
	Number state = max;
	while (count_bits(state) != weight)
	{
		state--;
	}
	return state;
}

// Перейтив следуещее состояние с тем же весом 
template<class Number>
inline Number next_state(Number state)
{
	size_t weight = count_bits(state);
	state++;
	while (count_bits(state) != weight)
	{
		state++;
	}
	return state;
}

//
//	Блок + конфигурация блока
//

// Создать блок из массива с использованием конфигурации
template<class Iterator>
Block create_block(Iterator arr, const BlockConfiguration & conf)
{
	Block block(conf.size());
	memset(block.data(), 0, block.size());
	
	size_t index = 0;
	for (size_t i = 0; i < block.size(); i++)
	{
		if (conf[i])
		{
			block[i] = arr[index];
			index++;
		}
	}

	return block;
}

// Обновить блок из массива с использованием конфигурации
template<class Iterator>
void update_block(Iterator arr, const BlockConfiguration & conf, Block & block)
{
	// Проверка соответсвия размеров блока и конфигурации
	assert(block.size() == conf.size());

	memset(block.data(), 0, block.size());

	size_t index = 0;
	for (size_t i = 0; i < block.size(); i++)
	{
		if (conf[i])
		{
			block[i] = arr[index];
			index++;
		}
	}
}


// Извлечь элементы блокa по конфигурации
template<class Iterator>
void extract_elements_from_block(const Block & block, const BlockConfiguration & conf, Iterator arr)
{
	// Проверка соответсвия размеров блока и конфигурации
	assert(block.size() == conf.size());

	size_t index = 0;

	for (size_t i = 0; i < conf.size(); i++)
	{
		if (conf[i])
		{
			arr[index] = block[i];
			index++;
		}
	}
}


//
//	Путь + конфигурация пути
//

// Создать путь из массива в сответствии конфигурацией
template<class Iterator>
Path create_path(const Iterator arr, const PathConfiguration & conf)
{
	size_t input_weight = conf.input_configuration().weight();

	Path path(
		create_block(arr, conf.input_configuration()), 
		create_block(arr + input_weight, conf.output_configuration())
	);
	
	return path;
}


// Обновить путь из массива в сответствии конфигурацией
template<class Iterator>
void update_path(const Iterator arr, const PathConfiguration & conf, Path & path)
{
	size_t input_weight = conf.input_configuration().weight();

	update_block(arr, conf.input_configuration(), path.input());
	update_block(arr + input_weight, conf.output_configuration(), path.output());
}


// Извлечь элементы пути по конфигурации
template<class Iterator>
void extract_elements_from_path(const Path & path, const PathConfiguration & conf, Iterator arr)
{
	size_t input_weight = conf.input_configuration().weight();

	extract_elements_from_block(path.input(), conf.input_configuration(), arr);
	extract_elements_from_block(path.output(), conf.output_configuration(), arr + input_weight);
}


//
//	Блок + линейное преобразование
//

// Создать блок применив линейное преобразование
Block create_block(const Block & input, const LinearTransform & l_transform);

// Обновить блок применив линейное преобразование
void update_block(const Block & input, const LinearTransform & l_transform, Block & output);


//
//	Путь + конфигурация пути + линейное преобразование
//

// Создать путь из решения системы линейных уравнений сответствии конфигурацией
template<class Iterator>
Path create_path(const Iterator arr, const PathConfiguration & conf, const LinearTransform & l_transform)
{
	size_t input_weight = conf.input_configuration().weight();
	Block input = create_block(arr, conf.input_configuration());
	Block output = create_block(input, l_transform);

	return Path(std::move(input), std::move(output));
}

// Обновить путь из решения системы линейных уравнений сответствии конфигурацией
template<class Iterator>
void update_path(const Iterator arr, const PathConfiguration & conf, const LinearTransform & l_transform, Path & path)
{
	update_block(arr, conf.input_configuration(), path.input());
	update_block(path.input(), l_transform, path.output());
}


//
// Дифференциалы
//

// Узнать уровень после которого вероятность пути станет нулевой
size_t level_when_probability_will_zero(const Block & dx, const Path & path, const Block & dy, const TransitionProbability & t_probability);

// Удалить пути с нулевой вероятностью из дифференциала
void delete_bad_paths_from_differential(Differential & differential, const TransitionProbability & t_probability);
