#pragma once
#include "../byte_vector.h"
#include "../block.h"
#include "../configuration/path_configuration.h"

class Path
{
public:
	// Конструктор по умолчанию
	Path() {}
	// Конструктор - устанвливает размеры блоков
	explicit Path(size_t size) : input_block(size), output_block(size)
	{

	}
	// Конструктор - принимающий готовые блоки
	Path(const Block & input, const Block & output) : input_block(input), output_block(output)
	{

	}
	// Перемещающий конструктор - принимающий готовые блоки
	Path(Block && input, Block && output) : input_block(std::move(input)), output_block(std::move(output))
	{

	}
	// Копирующий конструктор
	Path(const Path & other) :
		input_block(other.input_block),
		output_block(other.output_block)
	{
	}
	// Перемещающий конструктор
	Path(Path && other) :
		input_block(std::move(other.input_block)),
		output_block(std::move(other.output_block))
	{
	}
	// Оператор присваивания
	Path & operator=(const Path & other)
	{
		if (this == &other)
			return *this;

		input_block = other.input_block;
		output_block = other.output_block;
		return *this;
	}
	// Перемещающий оператор присваивания
	Path & operator=(Path && other)
	{
		input_block = std::move(other.input_block);
		output_block = std::move(other.output_block);
		return *this;
	}

	operator std::string() const
	{
		std::stringstream ss;
		ss << "{ " << std::endl;
		ss << '\t' << static_cast<std::string>(input_block) << std::endl;
		ss << '\t' << static_cast<std::string>(output_block) << std::endl;
		ss << "}";
		return ss.str();
	}

	// Получить размер блоков
	size_t block_size() const
	{
		return input_block.size();
	}

	// Оператор сравнения
	bool operator==(Path op2) const
	{
		return bool(this->input_block == op2.input_block && this->output_block == op2.output_block);
	}
	bool operator!=(Path op2) const
	{
		return !this->operator==(op2);
	}

	// Функции доступа к блокам
	Block & input()
	{
		return input_block;
	}
	const Block & input() const
	{
		return input_block;
	}
	Block & output()
	{
		return output_block;
	}
	const Block & output() const
	{
		return output_block;
	}

private:
	Block input_block;
	Block output_block;
};
