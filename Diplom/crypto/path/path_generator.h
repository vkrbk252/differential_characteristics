#pragma once
#include <vector>
#include "path.h"
#include "../functions.h"
#include "../configuration/path_configuration.h"
#include "../rational_number.h"
#include "../linear_system.h"
#include "../linear_transform.h"
#include "../transition_probability.h"

class PathGenerator
{
public:
	PathGenerator(const LinearTransform & l_transf) : 
		l_transform(l_transf),
		conf(l_transf.block_size()),
		solution_arr(l_transf.block_size())
	{
		
	}

	// Сгенерировать все решения
	template<class Number>
	void generate_all(Number state)
	{
		conf.set_state(state);
		l_system.decide_system(conf, l_transform.get_l_matrix(), l_transform.block_size());
		size_t count_free_vars = l_system.count_free_vars();

		// Перевыделяем память если нужно
		if (paths.size() != 255 * count_free_vars)
		{
			paths.resize(255 * count_free_vars);
			free_vars.resize(count_free_vars);
		}

		// Устанавливаем начальное состояние
		for (size_t i = 0; i < free_vars.size(); i++)
		{
			free_vars[i] = 1;
		}

		// Генерируем все решения
		for (size_t i = 0; i < paths.size(); i++)
		{
			l_system.get_solution(free_vars.data(), solution_arr.data());
			update_path(solution_arr.data(), conf, l_transform, paths[i]);
			next_free_vars_state();
		}
	}

	// Получить константную ссылку на массив путей
	const std::vector<Path> & get_paths() const
	{
		return paths;
	}

	// Получить конфигурацию
	const PathConfiguration & get_conf() const
	{
		return conf;
	}
private:
	std::vector<Path> paths;
	const LinearTransform & l_transform;
	PathConfiguration conf;
	LinearSystem l_system;
	std::vector<uint8_t> free_vars;
	std::vector<uint8_t> solution_arr;

	// Следующая конфигурация свободных переменных
	void next_free_vars_state()
	{
		for (size_t i = 0; i < free_vars.size(); i++)
		{
			if (free_vars[i] != 255)
			{
				free_vars[i]++;
				break;
			}
			
			free_vars[i] = 1;
			continue;
		}
	}
};
