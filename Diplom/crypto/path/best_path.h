#pragma once
#include <vector>
#include "path.h"
#include "../rational_number.h"
#include "../transition_probability.h"

class BestPaths
{
public:
	// Инициализировать хранилище лучших путей
	BestPaths(size_t size_pool, const TransitionProbability & t_prob) :
		best(0),
		best_paths(size_pool),
		saved_paths(0), 
		total_paths(0),
		pool_size(size_pool),
		t_probability(t_prob)
	{
		
	}

	// Добавить дополнительные пути
	void add(const Path & path)
	{
		RationalNumber current = t_probability.max_path_probability(path);
		if (current > best)
		{
			best = current;
			saved_paths = 0;
			total_paths = 0;
			save_path(path);
		}
		else if (current == best)
		{
			save_path(path);
		}
	}
	void add(const std::vector<Path> & paths)
	{
		for (size_t i = 0; i < paths.size(); i++)
		{
			add(paths[i]);
		}
	}
	void add(const BestPaths & other)
	{
		if (best < other.best)
		{
			best = other.best;
			saved_paths = 0;
			total_paths = 0;
			for (size_t i = 0; i < other.saved_paths; i++)
			{
				add(other.best_paths[i]);
			}
			total_paths = other.total_paths;
		}
		else if (best == other.best)
		{
			for (size_t i = 0; i < other.saved_paths; i++)
			{
				add(other.best_paths[i]);
			}
			total_paths += other.total_paths - other.saved_paths;
		}
	}

	// Получить вероятность лучшего пути
	RationalNumber get_best_probability() const
	{
		return best;
	}
	// Получить сохраненные лучшие пути
	std::vector<Path> get_best_paths() const
	{
		std::vector<Path> result(saved_paths);
		for (size_t i = 0; i < saved_paths; i++)
		{
			result[i] = best_paths[i];
		}
		return result;
	}
	// Общее количество путей
	size_t total_count() const
	{
		return total_paths;
	}
	// Сохраненное количество путей
	size_t saved_count() const
	{
		return saved_paths;
	}
	// Размер хранилища
	size_t pool_count() const
	{
		return pool_size;
	}

	// Сбросить внутреннее состояние
	void set_best_probability(RationalNumber new_best)
	{
		saved_paths = 0;
		total_paths = 0;
		best = new_best;
	}

private:
	RationalNumber best;
	std::vector<Path> best_paths;
	size_t saved_paths;
	size_t total_paths;
	size_t pool_size;

	const TransitionProbability & t_probability;

	// Сохранить путь в массив лучших путей
	void save_path(const Path & path)
	{
		if (saved_paths < pool_size)
		{
			best_paths[saved_paths] = path;
			saved_paths++;
			total_paths++;
		}
		else
		{
			total_paths++;
		}
	}

};
