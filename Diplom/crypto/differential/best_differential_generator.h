#pragma once
#include <vector>
#include "differential.h"
#include "../functions.h"
#include "../configuration/path_configuration.h"
#include "../rational_number.h"
#include "../linear_system.h"
#include "../linear_transform.h"
#include "../transition_probability.h"

class BestDifferentialGenerator
{
public:
	BestDifferentialGenerator(RationalNumber threshold_probability, const TransitionProbability & t_probability) : 
		threshold(threshold_probability),
		transition_probability(t_probability)
	{

	}

	// Установить пороговую вероятность
	void set_threshold_probability(RationalNumber threshold_probability)
	{
		threshold = threshold_probability;
	}
	// Получить пороговую вероятность
	RationalNumber get_threshold_probability() const
	{
		return threshold;
	}

	// Найти лучшие дифференциалы
	std::vector<Differential> find_differentials(const PathConfiguration & conf, const std::vector<Path> & paths_vector)
	{
		result.resize(0);
		block_size = conf.input_configuration().size();
		paths = paths_vector;
		
		// Устанавливаем максимальные значения вероятностей для каждого из путей
		probabilities.resize(paths.size());
		for (size_t i = 0; i < paths.size(); i++)
		{
			probabilities[i] = transition_probability.max_path_probability(paths[i]);
		}

		// Создаем массив ссылок на пути
		std::vector<size_t> path_indexes;
		for (size_t i = 0; i < paths.size(); i++)
		{
			path_indexes.push_back(i);
		}

		// Ищем все дифференциалы
		find_differential(path_indexes, conf.nz_begin());
		
		return result;
	}
private:
	RationalNumber threshold;
	const TransitionProbability & transition_probability;
	Block delta_x;
	Block delta_y;
	size_t block_size;
	std::vector<Differential> result;
	std::vector<Path> paths;
	std::vector<RationalNumber> probabilities;

	using level_type = PathConfiguration::nz_iterator;

	// Доступ к счетчику через итератор
	Block::reference counter_access(const level_type & level)
	{
		assert(level.get_index() != -1 && level.get_index() != block_size * 2);

		if (level.get_index() < block_size)
			return delta_x[level.get_index()];
		else
			return delta_y[level.get_index() - block_size];
	}
	// Доступ к элементу пути через итератор
	Block::reference path_access(size_t path_index, const level_type & level)
	{
		if (level.get_index() < block_size)
			return paths[path_index].input()[level.get_index()];
		else
			return paths[path_index].output()[level.get_index() - block_size];

	}

	// Конкретная вероятность перехода
	RationalNumber get_probability_for_level(size_t path_index, const level_type & level)
	{
		if (level.get_index() < block_size)
			return transition_probability.dX_sbox_dY(counter_access(level), path_access(path_index, level));
		else
			return transition_probability.dX_sbox_dY(path_access(path_index, level), counter_access(level));
	}
	// Максимальная вероятность перехода
	RationalNumber get_max_probability_for_level(size_t path_index, const level_type & level)
	{
		if (level.get_index() < block_size)
			return transition_probability.max_to_dY(path_access(path_index, level));
		else
			return transition_probability.max_from_dX(path_access(path_index, level));
	}
	
	// Подставить конкретную вероятность перехода
	void update_probability(size_t path_index, const level_type & level)
	{
		probabilities[path_index] /= get_max_probability_for_level(path_index, level);
		probabilities[path_index] *= get_probability_for_level(path_index, level);
	}
	// Подставить максимальную вероятность перехода
	void reset_probability(size_t path_index, const level_type & level)
	{
		probabilities[path_index] /= get_probability_for_level(path_index, level);
		probabilities[path_index] *= get_max_probability_for_level(path_index, level);
	}

	// Сохранить дифференциал
	void save_differencial(const std::vector<size_t> & path_indexes)
	{
		Differential diff;
		diff.get_delta_x() = delta_x;
		diff.get_delta_y() = delta_y;
		diff.get_paths().resize(path_indexes.size());
		for (size_t i = 0; i < path_indexes.size(); i++)
		{
			diff.get_paths()[i] = paths[path_indexes[i]];
		}
		RationalNumber prob = transition_probability.differential_probability(diff);
		if (prob > threshold)
		{
			threshold = prob;
			result.clear();
			result.push_back(std::move(diff));
		}
		
		if (prob == threshold)
		{
			result.push_back(std::move(diff));
		}
	}
	

	// Рекурсивный поиск дифференциалов
	void find_differential(const std::vector<size_t> & path_indexes, const level_type & level)
	{
		// Сохранение найденного дифференциала и выход из рекурсии
		if (level.get_index() == block_size * 2)
		{
			save_differencial(path_indexes);
			return;
		}

		// Перебираем все возможные варианты на данном уровне
		counter_access(level) = 255;
		do
		{
			// Оцениваем дифференциал на текущем уровне и собираем пути для рекурсивного вызова
			std::vector<size_t> new_paths;
			RationalNumber total(0);
			for (size_t i = 0; i < path_indexes.size(); i++)
			{
				RationalNumber curr = get_probability_for_level(path_indexes[i], level);
				if (curr > 0)
				{
					update_probability(path_indexes[i], level);
					new_paths.push_back(path_indexes[i]);
					total += probabilities[path_indexes[i]];
				}
			}

			// Проверяем нужно ли завершить рекурсию
			if (total < threshold)
			{
				// Откат изменений
				for (size_t i = 0; i < new_paths.size(); i++)
				{
					reset_probability(new_paths[i], level);
				}
				// Переход на следующую итерацию
				counter_access(level)--;
				continue;
			}

			// Рекурсивный вызов
			find_differential(new_paths, level + 1);
			
			// Откат изменений
			for (size_t i = 0; i < new_paths.size(); i++)
			{
				reset_probability(new_paths[i], level);
			}

			// Переход на следующую итерацию
			counter_access(level)--;

		} while (counter_access(level) > 0);

		return;
	}
};

