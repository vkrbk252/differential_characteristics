#pragma once
#include <vector>
#include "../block.h"
#include "../path/path.h"

class Differential
{
public:
	// Конструктор по умолчанию
	Differential() {}
	// Копирующий конструктор
	Differential(const Differential & other) :
		delta_x(other.delta_x),
		paths(other.paths),
		delta_y(other.delta_y)
	{}
	// Перемещающий конструктор
	Differential(Differential && other) :
		delta_x(std::move(other.delta_x)),
		paths(std::move(other.paths)),
		delta_y(std::move(other.delta_y))
	{}

	// Оператор присваивания
	Differential & operator=(const Differential & other)
	{
		if (this == &other)
			return *this;

		delta_x = other.delta_x;
		paths = other.paths;
		delta_y = other.delta_y;
		return *this;
	}
	// Перемещающий оператор присваивания
	Differential & operator=(Differential && other)
	{
		delta_x = std::move(other.delta_x);
		paths = std::move(other.paths);
		delta_y = std::move(other.delta_y);
	}

	operator std::string() const
	{
		std::stringstream ss;
		ss << "{ " << std::endl;
		ss << '\t' << "delta_x = " << static_cast<std::string>(delta_x) << std::endl;
		ss << '\t' << "paths = {" << std::endl;
		for (size_t i = 0; i < paths.size(); i++)
		{
			ss << static_cast<std::string>(paths[i]) << ", " << std::endl;
		}
		ss << '\t' << "}" << std::endl;
		ss << '\t' << "delta_y = " << static_cast<std::string>(delta_y) << std::endl;
		ss << "}" << std::endl;
		return ss.str();
	}

	// Получить dX
	Block & get_delta_x()
	{
		return delta_x;
	}
	const Block & get_delta_x() const
	{
		return delta_x;
	}

	// Получить dX
	Block & get_delta_y()
	{
		return delta_y;
	}
	const Block & get_delta_y() const
	{
		return delta_y;
	}

	// Получить пути входящие в дифференциал
	std::vector<Path> & get_paths()
	{
		return paths;
	}
	const std::vector<Path> & get_paths() const
	{
		return paths;
	}

	// Количество путей входящих в дифференциал
	size_t count_paths() const
	{
		return paths.size();
	}

private:
	Block delta_x;
	std::vector<Path> paths;
	Block delta_y;
};
