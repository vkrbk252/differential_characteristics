#pragma once
#include <vector>
#include "differential.h"
#include "../rational_number.h"
#include "../transition_probability.h"

class BestDifferential
{
public:
	// Инициализировать хранилище лучших путей
	BestDifferential(size_t size_pool, const TransitionProbability & t_prob) :
		best(0),
		best_differentials(size_pool),
		saved_differentials(0),
		total_differentials(0),
		pool_size(size_pool),
		t_probability(t_prob)
	{

	}

	// Добавить дополнительные дифференциалы
	void add(const Differential & differential)
	{
		RationalNumber current = t_probability.differential_probability(differential);
		if (current > best)
		{
			best = current;
			saved_differentials = 0;
			total_differentials = 0;
			save_differential(differential);
		}
		else if (current == best)
		{
			save_differential(differential);
		}
	}
	void add(const std::vector<Differential> & differentials)
	{
		for (size_t i = 0; i < differentials.size(); i++)
		{
			add(differentials[i]);
		}
	}
	void add(const BestDifferential & other)
	{
		if (best < other.best)
		{
			best = other.best;
			saved_differentials = 0;
			total_differentials = 0;
			for (size_t i = 0; i < other.saved_differentials; i++)
			{
				add(other.best_differentials[i]);
			}
			total_differentials = other.total_differentials;
		}
		else if (best == other.best)
		{
			for (size_t i = 0; i < other.saved_differentials; i++)
			{
				add(other.best_differentials[i]);
			}
			total_differentials += other.total_differentials - other.saved_differentials;
		}
	}

	// Получить вероятность лучшего дифференциала
	RationalNumber get_best_probability() const
	{
		return best;
	}
	// Получить сохраненные дифференциалы
	std::vector<Differential> get_best_differentials() const
	{
		std::vector<Differential> result(saved_differentials);
		for (size_t i = 0; i < saved_differentials; i++)
		{
			result[i] = best_differentials[i];
		}
		return result;
	}
	// Общее количество дифференциалов
	size_t total_count() const
	{
		return total_differentials;
	}
	// Сохраненное количество дифференциалов
	size_t saved_count() const
	{
		return saved_differentials;
	}
	// Размер хранилища
	size_t pool_count() const
	{
		return pool_size;
	}

	// Сбросить внутреннее состояние
	void set_best_probability(RationalNumber new_best)
	{
		saved_differentials = 0;
		total_differentials = 0;
		best = new_best;
	}

private:
	RationalNumber best;
	std::vector<Differential> best_differentials;
	size_t saved_differentials;
	size_t total_differentials;
	size_t pool_size;

	const TransitionProbability & t_probability;

	// Сохранить дифференциал в массив лучших дифференциалов
	void save_differential(const Differential & differential)
	{
		if (saved_differentials < pool_size)
		{
			best_differentials[saved_differentials] = differential;
			saved_differentials++;
			total_differentials++;
		}
		else
		{
			total_differentials++;
		}
	}

};
