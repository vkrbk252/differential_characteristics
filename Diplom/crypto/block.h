#pragma once
#include <vector>
#include <string>
#include <sstream>
#include <cstring>


class Block
{
public:
	// Тип хранимых элементов
	using value_type = uint8_t;
	// Тип для хранения размера
	using size_type = size_t;
	// Тип ссылки на элемент
	using reference = value_type & ;
	// Тип константной ссылки на элемент
	using const_reference = const value_type &;

	// Конструктор по умолчанию
	Block() : block(default_block_size) {}
	// Конструктор задающий размер блока
	explicit Block(size_type block_size) : block(block_size) {}
	// Конструктор с инициализацией
	Block(size_type block_size, const value_type & value) : block(block_size, value) {}
	// Конструктор принимающий инициализирующий список
	Block(std::initializer_list<value_type> init) : block(init) {}
	// Копирующий конструктор
	Block(const Block & other) : block(other.block)
	{

	}
	// Перемещающий конструктор
	Block(Block && other) : block(std::move(other.block))
	{

	}

	Block & operator=(const Block & other)
	{
		if (this == &other)
			return *this;

		block = other.block;
		return *this;
	}
	Block & operator=(Block && other)
	{
		block = std::move(other.block);
		return *this;
	}

	operator std::string() const
	{
		std::stringstream ss;
		ss << "{ ";
		for (size_t i = 0; i < block.size(); i++)
		{
			ss << uint8_t_to_hexstr(block[i]) << ", ";
		}
		ss << " }";
		return ss.str();
	}

	// Получить указатель на данные
	value_type * data()
	{
		return block.data();
	}
	const value_type * data() const
	{
		return block.data();
	}

	// Произвольный доступ к элементам
	reference operator[](size_type index)
	{
		return block[index];
	}
	const_reference operator[](size_type index) const
	{
		return block[index];
	}

	// Получить размер блока
	size_type size() const
	{
		return block.size();
	}

	// Оператор сравнения
	bool operator==(const Block & op2) const
	{
		if (this->size() != op2.size())
			return false;

		return !memcmp(this->data(), op2.data(), this->size());
	}
	bool operator!=(const Block & op2) const
	{
		return !this->operator==(op2);
	}

	// Установить размер блока по умолчанию
	static void set_default_block_size(size_t size)
	{
		default_block_size = size;
	}
	// Получить размер блока по умолчанию
	static size_t get_default_block_size()
	{
		return default_block_size;
	}

private:
	std::vector<value_type> block;
	static size_t default_block_size;

	// Вывод в строку hex значения uint8_t
	std::string uint8_t_to_hexstr(uint8_t value) const
	{
		std::stringstream ss;
		ss << std::hex << static_cast<int>(value >> 4) << static_cast<int>(value & 0x0f);
		return ss.str();
	}
};
