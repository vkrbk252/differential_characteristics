#pragma once
#include <cstdint>
#include <initializer_list>
#include <vector>

class Sbox
{
public:
	// Тип хранимых элементов
	using value_type = uint8_t;
	// Тип для хранения размера
	using size_type = size_t;
	// Тип константной ссылки на элемент
	using const_reference = const value_type &;

	// Конструктор принимающий массив
	Sbox(const value_type * data, size_type size) : sbox(data, data + size)
	{

	}

	// Произвольный доступ к элементам (read only)
	const_reference operator[](size_type index) const
	{
		return sbox[index];
	}

	// Получить размер блока
	size_type size() const
	{
		return sbox.size();
	}

private:
	const std::vector<value_type> sbox;

};
