#pragma once
#include <cstdint>
#include <vector>

class Gf2Polynom
{
public:
	// Конструктор по умолчанию
	Gf2Polynom() : value(0)
	{
	}

	// Конструктор преобразования
	Gf2Polynom(uint8_t value) : value(value)
	{
	}

	// Обратное преобразование
	inline operator uint8_t()
	{
		return value;
	}


	/// Операторы
	// Оператор +
	inline Gf2Polynom operator+(Gf2Polynom b) const
	{
		return value ^ b.value;
	}
	inline Gf2Polynom operator-(Gf2Polynom b) const
	{
		return value ^ b.value;
	}

	// Оператор *
	inline Gf2Polynom operator*(Gf2Polynom b) const
	{
		return mul_table[value * 256 + b.value];
	}
	// Оператор /
	inline Gf2Polynom operator/(Gf2Polynom b) const
	{
		return mul_table[value * 256 + inverse_table[b.value]];
	}

	// Сложить два элемента
	static inline uint8_t sum(uint8_t a, uint8_t b)
	{
		return a ^ b;
	}
	// Умножить два элемента
	static inline uint8_t mul(uint8_t a, uint8_t b)
	{
		return mul_table[a * 256 + b];
	}

	// Задать значение модуля
	static void set_module(size_t new_module)
	{
		module = new_module;
		calculate_multiply_table();
		calculate_inverse_table();
	}
	// Получить значение модуля
	static size_t get_module()
	{
		return module;
	}

private:
	static size_t module;
	static uint8_t mul_table[256 * 256];
	static uint8_t inverse_table[256];

	uint8_t value;


	// Привести полином по модулю поля
	static uint8_t gf2_mod(size_t polynom)
	{
		size_t degree_polynom = degree(polynom);
		size_t degree_module = degree(module);

		while (degree_polynom >= degree_module)
		{
			polynom ^= (module << (degree_polynom - degree_module));
			degree_polynom = degree(polynom);
		}

		return static_cast<uint8_t>(polynom);
	}

	// Степень многочлена
	static size_t degree(size_t polynom)
	{
		size_t i = 0;
		while (((polynom >> i) | 0x01) != 0x01)
		{
			i++;
		}
		return i;
	}

	// Умножение элементов
	static uint8_t gf2_mul(uint8_t a, uint8_t b)
	{
		size_t c = 0;
		size_t max_degree = std::max(degree(a), degree(b)) + 1;

		for (size_t i = 0; i < max_degree; i++)
		{
			if (((b >> i) & 0x01) == 0x01)
				c ^= a << i;
		}

		return gf2_mod(c);
	}

	// Пересчитать таблицу умножения
	static void calculate_multiply_table()
	{
		// Строим таблицу умножения
		for (size_t i = 0; i < 256; i++)
		{
			for (size_t j = 0; j < 256; j++)
			{
				mul_table[i * 256 + j] = gf2_mul(static_cast<uint8_t>(i), static_cast<uint8_t>(j));
			}
		}
	}

	// Пересчитать таблицу обратных элементов
	static void calculate_inverse_table()
	{
		// Обратный элемент определен только для ненулевых элементов
		for (size_t i = 1; i < 256; i++)
		{
			// Ищем в строке единицу
			size_t index = 0;
			for (size_t j = 1; j < 256; j++)
			{
				if (mul_table[i * 256 + j] == 1)
				{
					index = j;
					break;
				}
			}

			inverse_table[i] = static_cast<uint8_t>(index);
		}
	}
};


