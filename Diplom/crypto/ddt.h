#pragma once
#include <vector>

#include "sbox.h"


class Ddt
{
public:
	// Тип хранимых элементов
	using delta_type = uint8_t;
	// Тип для хранения размера
	using size_type = size_t;
	// Любой элемент
	class any_type { };
	any_type any;

	// Конструктор принимающий sbox
	explicit Ddt(const Sbox & sbox) :
		ddt(256 * 256),
		max_x(sbox.size()),
		max_y(sbox.size())
	{
		calculate_ddt(sbox);
		calculate_maximums();
	}

	// Получить максимальное количество переходов из dX в dY
	inline size_type operator()(delta_type dX, delta_type dY) const
	{
		return ddt[dX * 256 + dY];
	}
	inline size_type operator()(any_type any, delta_type dY) const
	{
		return max_y[dY];
	}
	inline size_type operator()(delta_type dX, any_type any) const
	{
		return max_x[dX];
	}
	inline size_type operator()(any_type any1, any_type any2) const
	{
		return total();
	}


	// Размер sbox (максимальнов возможное кол-во переходов)
	inline size_type total() const
	{
		return max_x.size();
	}

private:
	// Расчет DDT
	void calculate_ddt(const Sbox & sbox)
	{
		delta_type delta_x;
		for (size_t x1 = 0; x1 < sbox.size(); x1++)
		{
			for (size_t x2 = 0; x2 < sbox.size(); x2++)
			{
				delta_x = static_cast<delta_type>(x1 ^ x2);
				ddt[delta_x * 256 + (sbox[x1] ^ sbox[x2])]++;
			}
		}
	}

	// Расчет максимумов
	void calculate_maximums()
	{
		// Для перехода ИЗ
		for (size_t delta_x = 0; delta_x < total(); delta_x++)
		{
			size_t cur_max_x = 0;
			for (size_t i = 0; i < total(); i++)
			{
				if (cur_max_x < ddt[delta_x * 256 + i])
					cur_max_x = ddt[delta_x * 256 + i];
			}
			max_x[delta_x] = cur_max_x;
		}

		// Для перехода В
		for (size_t delta_y = 0; delta_y < total(); delta_y++)
		{
			size_t cur_max_y = 0;
			for (size_t i = 0; i < total(); i++)
			{
				if (cur_max_y < ddt[i * 256 + delta_y])
					cur_max_y = ddt[i * 256 + delta_y];
			}
			max_y[delta_y] = cur_max_y;
		}
	}


	// DDT таблица, содержит количество переходов из delta_x в delta_y
	std::vector<size_t> ddt;

	// Максимально возможное количество переходов из/в для каждого ряда/столбца
	std::vector<size_t> max_x;
	std::vector<size_t> max_y;

};
