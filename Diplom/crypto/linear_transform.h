#pragma once
#include "gf2_polynom.h"

// Линейное преобразование
class LinearTransform
{
public:
	// Kонструктор преобразует arr -> matrix[block_length][block_length]
	LinearTransform(size_t block_size, const uint8_t * arr) :
		block_length(block_size)
	{
		for (size_t i = 0; i < block_size; i++)
		{
			for (size_t j = 0; j < block_size; j++)
			{
				l_matrix[i * block_size + j] = *arr;
				arr++;
			}
		}
	}

	// Выполнить линейное преобразование
	void operator()(const uint8_t * input, uint8_t * output) const
	{
		for (size_t i = 0; i < block_length; i++)
		{
			output[i] = 0;
			for (size_t j = 0; j < block_length; j++)
			{
				//output[i] = static_cast<uint8_t>(Gf2Polynom(output[i]) +
				//	Gf2Polynom(l_matrix[i * block_length + j]) * Gf2Polynom(input[j]));

				output[i] = Gf2Polynom::sum(output[i], Gf2Polynom::mul(l_matrix[i * block_length + j], input[j]));
			}
		}
	}

	// Выполнить линейное преобразование
	template<class Block>
	void operator()(const Block & input, Block & output) const
	{
		this->operator()(input.data(), output.data());
	}

	// Получить размер блока
	size_t block_size() const
	{
		return block_length;
	}

	// Получить указатель на массив с матрицей линейного преобразованич
	const uint8_t * get_l_matrix() const
	{
		return l_matrix;
	}

private:
	size_t block_length;
	uint8_t l_matrix[256];
};
