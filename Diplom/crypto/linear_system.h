#pragma once
#include <vector>
#include <memory>
#include "configuration/path_configuration.h"


// Система линейных уравнений
class LinearSystem
{
public:
	// Конструктор принимающий конфигурацию пути и матрицу линейного преобразования
	LinearSystem() :
		reserved_size(0)
	{
	}

	// Конструктор принимающий матрицу представляющую систему линейных уравнений
	// Матрица elements размера rows * cols
	// Матрица elements будет модифицирована!
	LinearSystem(std::unique_ptr<uint8_t[]> elements, size_t count_rows, size_t count_cols) :
		arr(std::move(elements)),
		rows(count_rows),
		cols(count_cols),
		reserved_size(rows * cols)
	{
		transform_to_lower_triangular_view();
	}

	// Решить другую систему новую систему уравнений
	void decide_system(const PathConfiguration & path_conf, const uint8_t * linear_matrix, size_t block_size)
	{
		// Перевыделяем память если нужно
		if (reserved_size != block_size * block_size)
		{
			reserved_size = block_size * block_size;
			arr = std::unique_ptr<uint8_t[]>(new uint8_t[reserved_size]);
		}

		// Вычисляем размеры линейной матрицы
		cols = path_conf.input_configuration().weight();
		rows = path_conf.output_configuration().size() - path_conf.output_configuration().weight();

		// Копируем элементы из матрицы линейного преобразования в соответствии с конфигурацией
		size_t curr_i = 0;
		for (size_t i = 0; i < block_size; i++)
		{
			if (path_conf.output_configuration()[i] != 0)
				continue;

			size_t curr_j = 0;
			for (size_t j = 0; j < block_size; j++)
			{
				if (path_conf.input_configuration()[j] == 0)
					continue;

				arr[curr_i * cols + curr_j] = linear_matrix[i * block_size + j];
				curr_j++;
			}
			curr_i++;
		}

		// Приводим матрицу к треугольному виду
		transform_to_lower_triangular_view();
	}

	// Количество переменных
	size_t count_vars() const
	{
		return cols;
	}
	// Количество свободных переменных
	size_t count_free_vars() const
	{
		return cols - rows;
	}
	// Количество зависимых переменных 
	size_t count_depend_vars() const
	{
		return rows;
	}

	// Получить решение
	template<class InputIter, class OutputIter>
	void get_solution(InputIter free_vars, OutputIter result) const
	{
		// Копируем свободные переменные
		for (size_t i = 0; i < count_free_vars(); i++)
		{
			result[count_depend_vars() + i] = free_vars[i];
		}

		// Вычисляем остальные переменные
		for (int i = (int)rows - 1; i >= 0; i--)
		{
			result[i] = get_variable_value(i, result);
		}
	}

private:
	size_t rows;
	size_t cols;
	size_t reserved_size;
	std::unique_ptr<uint8_t[]> arr;

	// Доступ к элементам как в матрице
	uint8_t & matrix(size_t i, size_t j)
	{
		return arr[i * cols + j];
	}
	const uint8_t & matrix(size_t i, size_t j) const
	{
		return arr[i * cols + j];
	}

	// Привести матрицу к нижнетреугольному виду (матрица может быть не квадратной)
	void transform_to_lower_triangular_view()
	{
		// Количество диагональных элементов
		size_t count_diagonal_elements = std::min(rows, cols);

		// Для каждого элемента диагонали:
		for (size_t d = 0; d < count_diagonal_elements; d++)
		{
			// Для каждой строки
			for (size_t i = d; i < rows; i++)
			{
				// Умножаем все элементы на обратный
				Gf2Polynom inv_element = Gf2Polynom(1) / Gf2Polynom(matrix(i, d));
				for (size_t j = d; j < cols; j++)
				{
					matrix(i, j) = static_cast<uint8_t>(Gf2Polynom(matrix(i, j)) * inv_element);
				}

				// Начиная со второй строки, зануляем первый элемент
				if (i != d)
				{
					for (size_t j = d; j < cols; j++)
					{
						matrix(i, j) = static_cast<uint8_t>(
							Gf2Polynom(matrix(d, j)) + Gf2Polynom(matrix(i, j))
						);
					}
				}
			}
		}
	}

	// Получить значение следующей переменной по уже найденным
	template<class VarIt>
	uint8_t get_variable_value(size_t row_number, VarIt vars) const
	{
		Gf2Polynom result(0);

		for (size_t i = row_number + 1; i < cols; i++)
		{
			result = result + Gf2Polynom(matrix(row_number, i)) * Gf2Polynom(vars[i]);
		}

		return static_cast<uint8_t>(result);
	}
};
