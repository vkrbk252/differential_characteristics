#include "functions.h"

// Создать блок применив линейное преобразование
Block create_block(const Block & input, const LinearTransform & l_transform)
{
	// проверка соответсвия размеров блока используемого в линейном преобразовании
	assert(input.size() == l_transform.block_size());

	Block output(input.size());
	l_transform(input.data(), output.data());
	return output;
}

// Обновить блок применив линейное преобразование
void update_block(const Block & input, const LinearTransform & l_transform, Block & output)
{
	// проверка соответсвия размеров блока используемого в линейном преобразовании
	assert(input.size() == l_transform.block_size());
	assert(input.size() == output.size());

	l_transform(input.data(), output.data());
}

// Удалить пути с нулевой вероятностью из дифференциала
void delete_bad_paths_from_differential(Differential & differential, const TransitionProbability & t_probability)
{
	std::vector<Path> good_path;
	for (size_t i = 0; i < differential.count_paths(); i++)
	{
		if (t_probability.path_probability(
			differential.get_delta_x(),
			differential.get_paths()[i],
			differential.get_delta_y()))
		{
			good_path.push_back(std::move(differential.get_paths()[i]));
		}
	}
	differential.get_paths() = std::move(good_path);
}

// Узнать уровень после которого вероятность пути станет нулевой
size_t level_when_probability_will_zero(const Block & dx, const Path & path, const Block & dy, const TransitionProbability & t_probability)
{
	return t_probability.level_when_probability_will_zero(dx, path, dy);
	/*
	for (size_t i = 0; i < dx.size(); i++)
	{
		if (t_probability.dX_sbox_dY(dx[i], path.input()[i]) == 0)
		{
			return i;
		}
	}
	for (size_t i = 0; i < dy.size(); i++)
	{
		if (t_probability.dX_sbox_dY(path.output()[i], dy[i]) == 0)
		{
			return i + dx.size();
		}
	}
	*/
	//return dx.size() + dy.size();
}
