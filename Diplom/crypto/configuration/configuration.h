#pragma once
#include <vector>

// Конфигурация
	//  - Допустимые значения {0,1}
class Configuration
{
public:
	// Тип хранимых элементов
	using value_type = uint8_t;
	// Тип для хранения размера
	using size_type = size_t;
	// Тип ссылки на элемент
	using reference = uint8_t & ;
	// Тип константной ссылки на элемент
	using const_reference = const uint8_t &;

	// Конструктор задающий размер конфигурации
	Configuration(size_type size) : conf(size) {}

	// Произвольный доступ к элементам
	reference operator[](size_type index)
	{
		return conf[index];
	}
	const_reference operator[](size_type index) const
	{
		return conf[index];
	}

	// Получить размер комбинации
	size_type size() const
	{
		return conf.size();
	}

private:
	std::vector<uint8_t> conf;
};
