#pragma once
#include "block_configuration.h"
#include "../path/path.h"

class PathConfiguration
{
public:
	// Конструктор задающий размер конфигураций блоков
	PathConfiguration(size_t block_size) : input_block(block_size), output_block(block_size) {}

	// Получить конфигурацию входного блока
	BlockConfiguration & input_configuration()
	{
		return input_block;
	}
	const BlockConfiguration & input_configuration() const
	{
		return input_block;
	}

	// Получить конфигурацию выходного блока
	BlockConfiguration & output_configuration()
	{
		return output_block;
	}
	const BlockConfiguration & output_configuration() const
	{
		return output_block;
	}

	// Вес конфигурации (число ненулевых элементов)
	size_t weight() const
	{
		return input_block.weight() + output_block.weight();
	}

	// Преобразовать конфигурацию в число
	template<class Number>
	void get_state(Number & state) const
	{
		Number tmp;

		input_block.get_state(tmp);
		state = tmp;
		state <<= input_block.size();
		output_block.get_state(tmp);
		state |= tmp;
	}

	// Преобразовать число в конфигурацию
	template<class Number>
	void set_state(Number state)
	{
		input_block.set_state(state >> input_block.size());
		output_block.set_state(state);
	}

	// Преобразовать блоки в конфигурацию
	template<class Block>
	void set_state(const Block & in_block, const Block & out_block)
	{
		input_block.set_state(in_block.data());
		output_block.set_state(out_block.data());
	}

	// Итератор по ненулевым элементам
	class nz_iterator
	{
	public:
		nz_iterator(const PathConfiguration & path_conf, int cur_index) :
			conf(path_conf), index(cur_index)
		{

		}
		nz_iterator(const nz_iterator & other) : nz_iterator(other.conf, other.index)
		{

		}

		nz_iterator & operator=(const nz_iterator & other)
		{
			if (this == &other)
				return *this;

			this->index = other.index;
			return *this;
		}
		bool operator==(const nz_iterator & other) const
		{
			size_t block_size = conf.input_configuration().size();
			// Проверяем что оба итератора находяться в конечном состоянии
			if ((index == -1 || index == block_size * 2) && (other.index == -1 || other.index == block_size * 2))
				return true;

			// Данный код выполниться если хотя бы один из итераторов имеет действительное значение
			return bool(index == other.index);
		}
		bool operator!=(const nz_iterator & other) const
		{
			return !(*this == other);
		}

		// Получить номер элемента на который указывет итератор
		int get_index() const
		{
			return index;
		}

		// Смещение к следующему ненулевому элементу
		nz_iterator & operator++(int x)
		{
			int block_size = conf.input_configuration().size();
			for (int i = index + 1; i < block_size * 2; i++)
			{
				if (conf_access(i) != 0)
				{
					index = i;
					return *this;
				}
			}

			index = block_size * 2;
			return *this;
		}
		// Смещение к предыдущему ненулевому элементу
		nz_iterator & operator--(int x)
		{
			size_t block_size = conf.input_configuration().size();
			for (int i = index - 1; i >= 0; i--)
			{
				if (conf_access(i) != 0)
				{
					index = i;
					return *this;
				}
			}

			index = -1;
			return *this;
		}

		// Смещение на shift ненулевых элементов вперед
		nz_iterator operator+(size_t shift) const
		{
			nz_iterator result(*this);
			for (size_t i = 0; i < shift; i++)
			{
				result++;
			}
			return result;
		}
		// Смещение на shift ненулевых элементов назад
		nz_iterator operator-(size_t shift) const
		{
			nz_iterator result(*this);
			for (size_t i = 0; i < shift; i++)
			{
				result--;
			}
			return result;
		}

	private:
		const PathConfiguration & conf;
		int index;

		// Функция произвольного доступа к элементам конфигурации
		uint8_t conf_access(int i) const
		{
			size_t block_size = conf.input_configuration().size();
			if (i >= 0 && i < block_size * 2)
			{
				if (i < block_size)
					return conf.input_configuration()[i];
				else
					return conf.output_configuration()[i - block_size];
			}
			else
			{
				return 0;
			}
		}
	};

	// Получить итератор на первый ненулевой элемент
	nz_iterator nz_begin() const
	{
		nz_iterator result(*this, -1);
		result++;
		return result;
	}
	// Получить итератор на элемент за последним ненулевым
	nz_iterator nz_end() const
	{
		return nz_iterator(*this, input_block.size() * 2);
	}

private:
	BlockConfiguration input_block;
	BlockConfiguration output_block;
};
