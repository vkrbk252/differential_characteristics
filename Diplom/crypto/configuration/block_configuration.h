#pragma once
#include <type_traits>

#include "configuration.h"

#include "../block.h"

class BlockConfiguration : public Configuration
{
public:
	// Конструктор задающий размер конфигурации
	BlockConfiguration(size_type size) : Configuration(size) {}

	// Вес конфигурации (число ненулевых элементов)
	size_t weight() const
	{
		size_t result = 0;
		for (size_t i = 0; i < size(); i++)
		{
			if (operator[](i))
			{
				result++;
			}
		}
		return result;
	}

	// Преобразовать конфигурацию в число
	template<class Number>
	void get_state(Number & state) const
	{
		state = 0;
		for (size_t i = 0; i < size(); i++)
		{
			state <<= 1;
			if (operator[](i))
				state |= 0x01;
		}
	}

	// Преобразовать число в конфигурацию
	template<class Number>
	void set_state(Number state)
	{
		for (int i = (int)size() - 1; i >= 0; i--)
		{
			operator[](i) = (state >> (size() - i - 1)) & 0x01;
		}
	}

	// Преобразовать массив в конфигурацию
	template<class Iterator>
	void set_state(Iterator * block)
	{
		for (size_t i = 0; i < size(); i++)
		{
			if (block[i])
				operator[](i) = 1;
			else
				operator[](i) = 0;
		}
	}
};
