#include "gf2_polynom.h"

size_t Gf2Polynom::module(0);
uint8_t Gf2Polynom::mul_table[256 * 256] = {};
uint8_t Gf2Polynom::inverse_table[256] = { 0 };
