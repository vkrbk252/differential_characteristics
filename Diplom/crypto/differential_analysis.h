#pragma once
#include "crypter.h"
#include "gf2_polynom.h"

#include "block.h"
#include "sbox.h"
#include "linear_transform.h"
#include "ddt.h"
#include "transition_probability.h"
#include "path/path.h"
#include "linear_system.h"

// ќбъ€вление основных типов используемых в процессе нахождени€ дифференциальных характеристик 
class DifferentialAnalysis
{
public:
	using element_type = uint8_t;
	using probability_type = double;
	using polynom_type = Gf2Polynom;

	using block_type = Block;
	using sbox_type = Sbox;
	using linear_transform_type = LinearTransform;
	using ddt_type = Ddt;
	using transition_probability_type = TransitionProbability;
	using path_type = Path;
	using linear_system_type = LinearSystem;

	DifferentialAnalysis(const ByteCrypter & byte_crypter) :
		crypter(byte_crypter),
		sbox(byte_crypter.sbox, 256),
		linear_transform(byte_crypter.block_size, byte_crypter.l_matrix),
		ddt(sbox),
		transition_probability(sbox)
	{
		Gf2Polynom().set_module(byte_crypter.gf2_module);
		block_type().set_default_block_size(crypter.block_size);
	}

	const ByteCrypter crypter;
	const sbox_type sbox;
	const linear_transform_type linear_transform;
	const ddt_type ddt;
	const transition_probability_type transition_probability;
};
