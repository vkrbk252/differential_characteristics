#pragma once
#include "ddt.h"
#include "path/path.h"
#include "differential/differential.h"
#include "rational_number.h"

class TransitionProbability
{
public:
	using delta_type = typename Ddt::delta_type;
	using probability_type = RationalNumber;

	// Конструктор принимающий Sbox
	explicit TransitionProbability(const Sbox & sbox) : ddt(sbox) {}

	// Вероятность перехода из dX в dY через sbox
	probability_type dX_sbox_dY(delta_type dX, delta_type dY) const
	{
		return probability_type(ddt(dX, dY)) / probability_type(ddt.total());
	}
	probability_type dX_sbox_dY(const delta_type * dX, const delta_type * dY, size_t count) const
	{
		probability_type result(1);
		for (size_t i = 0; i < count; i++)
		{
			// Оптимизация. Если вероятность стала нулевой, то нет смысла дальше считать.
			//if (ddt(dX[i], dY[i]) == 0)
			//	return probability_type(0);
			
			result *= probability_type(ddt(dX[i], dY[i])) / probability_type(ddt.total());
			//result *= probability_type(ddt(dX[i], dY[i]));
		}
		return result;
		//return result / (probability_type(ddt.total()) * count);
	}

	// Максимальная вероятность перехода
	probability_type max_from_dX(delta_type dX) const
	{
		return probability_type(ddt(dX, ddt.any)) / probability_type(ddt.total());
	}
	probability_type max_from_dX(const delta_type * dX, size_t count) const
	{
		probability_type result(1);
		for (size_t i = 0; i < count; i++)
		{
			result *= probability_type(ddt(dX[i], ddt.any)) / probability_type(ddt.total());
		}
		return result;
	}
	probability_type max_to_dY(delta_type dY) const
	{
		return probability_type(ddt(ddt.any, dY)) / probability_type(ddt.total());
	}
	probability_type max_to_dY(const delta_type * dY, size_t count) const
	{
		probability_type result(1);
		for (size_t i = 0; i < count; i++)
		{
			result *= probability_type(ddt(ddt.any, dY[i])) / probability_type(ddt.total());
		}
		return result;
	}

	// Вероятность пути
	probability_type path_probability(const Block & dx, const Path & path, const Block & dy) const
	{
		return probability_type(
			dX_sbox_dY(dx.data(), path.input().data(), dx.size()) *
			dX_sbox_dY(path.output().data(), dy.data(), dy.size())
		);
	}
	// Найти вероятность пути, считая что элементы dXY до delim известны, а начиная с delim - любые
	probability_type bound_path_probability(const Block & dx, const Path & path, const Block & dy, size_t delim) const
	{
		probability_type result;
		if (delim < path.block_size())
		{
			result = dX_sbox_dY(dx.data(), path.input().data(), delim);
			result *= max_to_dY(path.input().data() + delim, path.block_size() - delim);
			result *= max_from_dX(path.output().data(), path.output().size());
		}
		else
		{
			delim -= path.block_size();
			result = dX_sbox_dY(dx.data(), path.input().data(), dx.size());
			result *= dX_sbox_dY(path.output().data(), dy.data(), delim);
			result *= max_from_dX(path.output().data() + delim, path.output().size() - delim);
		}
		return result;
	}
	// Найти максимальную вероятность пути
	probability_type max_path_probability(const Path & path) const
	{
		return probability_type(
			max_to_dY(path.input().data(), path.input().size())	*
			max_from_dX(path.output().data(), path.output().size())
		);
	}
	// Найти номер элемента пути, начиная с которого вероятность станет нулевой
	size_t level_when_probability_will_zero(const Block & dx, const Path & path, const Block & dy) const
	{
		for (size_t i = 0; i < dx.size(); i++)
		{
			if (ddt(dx[i], path.input()[i]) == 0)
			{
				return i;
			}
		}
		for (size_t i = 0; i < dy.size(); i++)
		{
			if (ddt(path.output()[i], dy[i]) == 0)
			{
				return i + dx.size();
			}
		}

		return dx.size() + dy.size();
	}

	// Вероятность дифференциала
	probability_type differential_probability(const Differential & differential) const
	{
		probability_type result = 0;
		for (size_t i = 0; i < differential.count_paths(); i++)
		{
			result += path_probability(
				differential.get_delta_x(), 
				differential.get_paths()[i], 
				differential.get_delta_y()
			);
		}
		return result;
	}

private:
	Ddt ddt;
};
