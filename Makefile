SOURCES=\
	Diplom/crypto/block.cpp \
	Diplom/crypto/functions.cpp \
	Diplom/crypto/gf2_polynom.cpp \
	Diplom/crypto/test.cpp \
	Diplom/main.cpp
	
CXX=gcc
CXXFLAGS= -std=c++1z  -o diplom 

#-lstdc++
 
all:
	$(CXX) $(CXXFLAGS) $(SOURCES)
